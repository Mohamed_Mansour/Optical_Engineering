%% A Method for Representation of Gaussian Beam Propagation 

%% the following code is for generating a Gaussian beam 
%% and how it spreads in 0-1000 um.
%% the following code is written by Mohamed Mansour Mohamed ElRayany 
%% as a part of the final project of ([ECE 681] Applied Optical Engineering),
%% under the supervision of Dr. Diaa Khalil in December 2015.

% Reset Matlab environment
clc; close; clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                         P A R T - I                             %%
%%                    G E N E R A T I O N                          %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x1 = -60;                           % Initial coordination 
x2 = 60;                            % Final coordination 
sn = 240;               			% Number of samples 
H = x2-x1;                          % Spacial domain size
del = H/sn;                     	% Samples spacing
x = linspace (x1, x2-del, sn);  	% Spacial domain
A = 1;                              % Amplitude of the beam
W0 = 8;                             % Beam waist radius
core = A*exp (-(x/W0).^2);          % Gaussian beam 
dftcore = fix(fft(core));           % FFT of the gaussian beam

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                       P A R T - II                              %%
%%                   P R O P A G A T I O N                         %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
s1 = 1:sn/2;            			% Region 1 of the phase
s2 = sn/2+1:sn;   					% Region 2 of the phase
s = [s1 s2];                        % Total regions
n = 1;                              % Refractive index of the medium
lambda = 0.8;                       % Lambda Value
L=[0 500 1000];                    	% Propagation Distances
k0=2*pi/lambda;             		% Wavenumber: k = 2*pi/Lambda
z=linspace(0,0,sn);     			% Dispacement in z
[x1,y1] = meshgrid ([x1:del:x2-del],[0:10:990]); 
for L = 0:10:990
    phase1 = n*k0*L*(1.*(1-0.5*(lambda^2*(s1-1).^2/H^2)));
    phase2 = n*k0*L*(1.*(1-0.5*(lambda^2*(sn+1-s2).^2/H^2)));
    phase =[phase1 phase2];			% Total phases
    newdft = abs(dftcore).*exp(i*(angle(dftcore)+(phase)));
    % Application of inverse Fourier transform
    newcore=ifft(newdft);
    % Generation the plotting matrix
    z1(L/10+1,:) = abs(newcore);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                        P A R T - III                            %%
%%                       P L O T T I N G                           %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting the magnitude of beams propagation
surf(x1,y1,z1);
shading interp;
colormap jet;
title('Magnitude of the gaussians beams propagation');
grid on;xlabel('x');ylabel('z');zlabel('y')
legend('height')